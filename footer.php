<?php wp_footer();?>
<div class="bg-color-custom">
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
            <div class="col">
                <h5>
                    Stay In The Know !
                </h5>
                <br>
                    <div class="input-group mb-3">
                        <input aria-describedby="button-addon2" aria-label="Recipient's username" class="form-control" placeholder="Enter Your Email" type="text">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" id="button-addon2" type="button">
                                    GO
                                </button>
                            </div>
                            <p>
                                To unsubscribe please
                                <a href="https://www.google.com">
                                    click here »
                                </a>
                            </p>
                        </input>
                    </div>
                </br>
            </div>
            <div class="col">
                <h5>
                    Lacus interdum
                </h5>
                <ul>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h5>
                    Lacus interdum
                </h5>
                <ul>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h5>
                    Lacus interdum
                </h5>
                <ul>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Lorem ipsum dolor
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Suspendisse in neque
                        </a>
                    </li>
                    <li>
                        <a href="https://www.google.com">
                            Praesent et eros
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="text-color-custom">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="copyright">
                    Copyright © 2013 Domain Name - All Right Reserved
                </p>
            </div>
            <div class="col-6 col-md-4">
                <p class="copyright">
                    Template by OS Templates
                </p>
            </div>
        </div>
    </div>
</div>
