<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                WLA
            </title>
            <?php wp_head();?>
        </meta>
    </head>
    <body <?php body_class();?>>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <button aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarDropdown" data-toggle="collapse" type="button">
                <span class="navbar-toggler-icon">
                </span>
            </button>
            <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>">
            </a>
            <div class="collapse navbar-collapse" id="navbarDropdown">
                <?php
        wp_nav_menu( array(
          'theme_location'  =>
                'navbar',
          'container'       => false,
          'menu_class'      => '',
          'fallback_cb'     => '__return_false',
          'items_wrap'      => '
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 %2$s" id="%1$s">
                    %3$s
                </ul>
                ',
          'depth'           => 2,
          'walker'          => new bootstrap_4_walker_nav_menu()
        ) );
      ?>
                <?php get_template_part('searchform'); ?>
            </div>
        </nav>
    </header>
</html>