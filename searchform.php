<?php
/**
 * Template for displaying search forms in tWPonB4
 *
 * @package WordPress
 * @subpackage tWPonB4
 * @since 1.0
 * @version 1.0
 */
?>
<?php $unique_id = esc_attr( uniqid( 'searchform' ) ); ?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="form-inline my-2 my-lg-0 searchform" id="searchform" method="get" role="search">
    <input class="form-control mr-sm-2" id="<?php echo $unique_id; ?>" name="s" placeholder="<?php echo esc_attr_x( 'Search this website …', 'placeholder', 'tWPonB4' ); ?>" type="text">
        <button class="btn btn-light my-2 my-sm-0" id="searchsubmit" type="submit">
            Search
        </button>
    </input>
</form>